
extends Node

export(int,"Player 1","Player 2") var device = 0

var rot
var turning

func _ready():
  reset()
  set_fixed_process(true)
  set_process_input(true)

func _input(event):
  if event.type == InputEvent.MOUSE_MOTION:
    rot += event.relative_pos.x/200
  if event.is_action_pressed("game_exit"):
    get_tree().quit()

func reset():
  rot = 0
  turning = false

func get_impulse(forward, rightways, impulse):
  # Keyboard input
  if Input.is_action_pressed("move_forward"):
    impulse += forward
  if Input.is_action_pressed("move_backward"):
    impulse -= forward
  if Input.is_action_pressed("move_leftways"):
    impulse -= rightways
  if Input.is_action_pressed("move_rightways"):
    impulse += rightways
  # Gamepad input
  var sidein = Input.get_joy_axis(device, JOY_AXIS_0)
  if abs(sidein) > 0.01:
    impulse += rightways*min(1,max(-1,sidein*sidein*sidein))
  var frontin = Input.get_joy_axis(device, JOY_AXIS_1)
  if abs(frontin) > 0.01:
    impulse -= forward*min(1,max(-1,frontin*frontin*frontin))
  return impulse

func get_impulse_3d(forward, rightways):
  return get_impulse(forward, rightways, Vector3(0,0,0))

func get_impulse_2d(forward, rightways):
  return get_impulse(forward, rightways, Vector2(0,0))

func is_turning():
  return turning;

func get_rot():
  var rotin = Input.get_joy_axis(device, JOY_AXIS_2)
  if abs(rotin) > .01:
    rot += min(1,max(-1,rotin*rotin*rotin))/40
    if not turning:
      turning = true
  elif turning:
    turning = false
  return rot
