
extends Node

onready var hud             = get_node("hud")
onready var teleport_target = get_node("teleport_target")
onready var gate            = get_node("gate")
onready var fade            = get_node("fade")
onready var timer           = get_node("timer")
onready var tuttimer        = get_node("tutorial_timer")
onready var player          = get_node("Player")
onready var vorpal             = get_node("/root/soundtrack")

var puzzle_count = 0
var fall_sfx
var gate_sfx
var speech

signal game_ready(player)
signal game_end()

func _ready():
  Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
  if vorpal.is_ok():
    set_process(true)
    set_process_input(true)
    for i in range(3):
      var statue = get_node("room_"+var2str(i+1))
      statue.connect("control_on", hud, "show_statue_control")
      statue.connect("control_on", self, "_tutorial", [], CONNECT_ONESHOT)
      statue.connect("control_off", hud, "hide_statue_control")
      statue.connect("success", hud, "show_alert", ["success"])
      statue.connect("failure", hud, "show_alert", ["failure"])
      statue.connect("success", self, "_puzzle_success")
      statue.set_teleport_target(teleport_target.get_translation())
    gate_sfx = vorpal.event_instance("gate-sfx")
    speech = vorpal.event_instance("speech")
    vorpal.push_command(speech, "intro")
    hud.show_alert("intro")
    emit_signal("game_ready", player)
  else:
    print("NOPE")

func _tutorial():
  if puzzle_count <= 0:
    tuttimer.connect("timeout", self, "_tutorial_speech", [], CONNECT_ONESHOT)
    tuttimer.start()

func _tutorial_speech():
  vorpal.push_command(speech, "tutorial")
  hud.show_alert("tutorial")

func _puzzle_success():
  puzzle_count += 1
  vorpal.push_command_1f(speech, "clear", puzzle_count)
  hud.show_alert(var2str(3-puzzle_count)+"left")
  if puzzle_count >= 3:
    gate.get_node("door").hide()
    gate.set_layer_mask(0)

func _the_end(_1, _2):
  timer.connect("timeout", self, "_finish")
  timer.start()
  fall_sfx = vorpal.event_instance("fall-sfx")
  vorpal.push_command(fall_sfx, "play")
  emit_signal("game_end")

func _finish():
  set_process(false)
  #vorpal.push_command(fall_sfx, "stop")
  vorpal.free_event(gate_sfx)
  vorpal.free_event(speech)
  for i in range(3):
    var statue = get_node("room_"+var2str(i+1))
    statue.finish()
  get_tree().change_scene("res://scenes/damnation_room.tscn")

func _process(delta):
  vorpal.set_event_position(gate_sfx, player.get_transform().inverse().basis*(gate.get_translation() - player.get_translation()))
  vorpal.tick(delta)
