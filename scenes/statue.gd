
extends Spatial

const CHARAS = ["mario", "tetris", "undertale"]

onready var main  = get_parent()
onready var door  = get_node("Door")
onready var vorpal   = get_node("/root/soundtrack")
onready var teleport_target = Vector3(0.0,0.0,0.0)
onready var grids  = [
  get_node("GridMap"),
  get_node("GridMap1"),
  get_node("GridMap2")
]

var markov
var player
var within
var playing
var solved
var selected

export(int, "mario", "tetris", "undertale") var character

signal control_on;
signal control_off;
signal success;
signal failure;

func _ready():
  get_parent().connect("game_ready", self, "_game_ready")
  selected = character
  for chara in get_node("Statue").get_children():
    if chara extends Sprite3D and chara.get_name() != CHARAS[character]:
      chara.hide()
  for i in range(grids.size()):
    if i != character:
      grids[i].queue_free()

func _game_ready(the_player):
  player = the_player
  within = false
  playing = false
  solved = false
  markov = [
    vorpal.event_instance("mario-main"),
    vorpal.event_instance("tetris-main"),
    vorpal.event_instance("undertale-main")
  ]
  play()
  set_process(true)
  printt("markov created:", markov[0], markov[1], markov[2])

func _on_player_enter_room(body):
  if vorpal != null and body == player:
    within = true
    vorpal.push_command(markov[selected], "unblock")

func _on_player_leave_room( body ):
  if vorpal != null and body == player:
    within = false
    vorpal.push_command(markov[selected], "block")

func _on_player_enter_control_zone( body ):
  if vorpal != null and body == player and not solved:
    print("entered control zone")
    stop()
    set_process_input(true)
    emit_signal("control_on")

func _on_player_leave_control_zone( body ):
  if vorpal != null and body == player:
    set_process_input(false)
    emit_signal("control_off")

func _input(event):
  if event.is_action_pressed("interact_1"):
    switch_theme(0)
  elif event.is_action_pressed("interact_2"):
    switch_theme(1)
  elif event.is_action_pressed("interact_3"):
    switch_theme(2)
  elif event.is_action_pressed("interact_4"):
    if playing:
      if selected == character:
        solved = true
        finish()
        emit_signal("control_off")
        emit_signal("success")
        player.teleport_to(teleport_target)
        return
      else:
        emit_signal("failure")
      stop()

func set_teleport_target(target):
  teleport_target = target

func play():
  playing = true
  vorpal.push_command(markov[selected], "start")
  if within:
    vorpal.push_command(markov[selected], "unblock")
  else:
    vorpal.push_command(markov[selected], "block")

func stop():
  playing = false
  if vorpal != null:
    vorpal.push_command(markov[selected], "stop")

func switch_theme(idx):
  stop()
  selected = idx
  play()

func finish():
  set_process_input(false)
  set_process(false)
  if vorpal != null:
    for theme in markov:
      vorpal.free_event(theme)
    door.finish()
    vorpal = null

func _process(delta):
  var pos = player.get_transform().inverse().basis*(get_translation() - player.get_translation())
  for theme in markov:
    vorpal.set_event_position(theme, pos)
