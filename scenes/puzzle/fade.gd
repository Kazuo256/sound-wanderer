
extends CanvasLayer

onready var curtain = get_node("curtain")
onready var tween   = get_node("Tween")

var player

func out(body):
  if player != null and body == player:
    tween.interpolate_method(curtain, "set_color", Color(0,0,0,0), Color(0,0,0,1), 0.5, Tween.TRANS_LINEAR, Tween.EASE_IN)
    tween.start()

func fadein():
  tween.interpolate_method(curtain, "set_color", Color(0,0,0,1), Color(0,0,0,0), 3, Tween.TRANS_LINEAR, Tween.EASE_IN)
  tween.start()

func set_color(color):
  curtain.set_color(color)

func _on_game_ready( the_player ):
  player = the_player
  set_process_input(true)

func _input(event):
  if event.is_action_pressed("devmode_1"):
    out(player)
