
extends Node

var vorpal
var status

func _ready():
  vorpal = VORPALModule.new()
  status = vorpal.start("patches")

func _exit_tree():
  vorpal.finish()

func is_ok():
  return status == true

func tick(delta):
  vorpal.tick(delta)

func event_instance(path):
  return vorpal.event_instance(path)

func free_event(id):
  vorpal.free_event(id)

func clear():
  vorpal.clear()

func push_command(id, cmd):
  vorpal.push_command(id, cmd)

func push_command_1f(id, cmd, arg):
  vorpal.push_command_1f(id, cmd, arg)

func set_event_position(id, pos):
  vorpal.set_event_position(id, pos.x, pos.y, pos.z)
