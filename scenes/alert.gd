
extends Control

export(String, MULTILINE) var text
export(float) var duration
export(bool)  var autostart = false
export(float) var delay

onready var timer = get_node("timer")
onready var msg   = get_node("msg")

func _ready():
  msg.set_text(text)
  if autostart:
    timer.connect("timeout", self, "go")
    timer.set_wait_time(delay)
    timer.start()

func go():
  show()
  if autostart:
    timer.disconnect("timeout", self, "go")
  timer.connect("timeout", self, "_on_time_up")
  timer.set_wait_time(duration)
  timer.start()

func _on_time_up():
  hide()
  timer.disconnect("timeout", self, "_on_time_up")
