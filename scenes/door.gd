
extends StaticBody

onready var vorpal  = get_node("/root/soundtrack")
onready var mask = get_layer_mask()

var player
var sfx

func _ready():
  _open()
  get_node("/root/main").connect("game_ready", self, "_game_ready")

func _game_ready(the_player):
  player = the_player
  sfx = vorpal.event_instance("door-sfx")
  set_process(true)

func _close():
  if is_hidden():
    vorpal.push_command(sfx, "play")
  show()
  set_layer_mask(mask)

func _open():
  hide()
  set_layer_mask(0)

func finish():
  set_process(false)
  vorpal.free_event(sfx)

func _process(delta):
  var pos = player.get_transform().inverse().basis*(get_translation() - player.get_translation())/6.0
  vorpal.set_event_position(sfx, pos)
