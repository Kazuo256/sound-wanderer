
extends CanvasLayer

onready var statue_control  = get_node("statue_control")

func show_statue_control():
  statue_control.show()

func hide_statue_control():
  statue_control.hide()

func show_alert(which):
  get_node(which + "_alert").go()
