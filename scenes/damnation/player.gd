
extends KinematicBody2D

const SPEED = 4.0
const PERIOD = 1.0/(2.0*144.0/60.0)


onready var control   = get_node("control")
onready var view      = get_node("view")
onready var particles = get_node("particles")
onready var tween     = get_node("tween")
onready var color     = view.get_color()

onready var moving = false

var highcolor
var dir = 0
var force = Vector2(0,0)

signal moving(dir)
signal stopped()

func set_ramp(col):
  var trans = col
  trans.a = 0
  particles.get_color_ramp().set_colors([col,col.linear_interpolate(trans,0.5), trans])
  particles.get_color_ramp().set_offsets([0.0, 0.7, 1.0])

func _ready():
  highcolor = color
  highcolor.h = 0.9
  set_ramp(color)
  set_fixed_process(true)

func is_moving():
  return moving

func get_dir():
  return 1 + dir

func set_force(f):
  force = f

func go_highcolor():
  view.set_color(highcolor)
  set_ramp(highcolor)

func go_dark(x):
  var col = highcolor.linear_interpolate(Color(0,0,0,1), x)
  view.set_color(col)
  set_ramp(col)

func finish():
  set_fixed_process(false)

func _fixed_process(delta):
  var forward = -get_transform().y
  var rightways = get_transform().x
  var impulse = control.get_impulse_2d(forward, rightways)
  if impulse.length_squared() > 0.0:
    view.set_rot(0)
    view.rotate(-(PI/2 + atan2(impulse.y, impulse.x)))
    dir = int(2.0*atan2(impulse.y, impulse.x)/PI)
    if not moving:
      emit_signal("moving", impulse)
      moving = true
  else:
    if moving:
      emit_signal("stopped")
      moving = false
  move(SPEED*impulse + force)
