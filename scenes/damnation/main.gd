
extends Node2D

const OBSTACLE_NUM    = 4

const Obstacle  = preload("res://scenes/damnation/obstacle.xscn")

const DARK_ZONE = 0
const COLOR_ZONE = 1
const FINAL_ZONE = 2

onready var bgimg     = get_node("bg/img")
onready var tween     = get_node("tween")
onready var player    = get_node("player")
onready var fade      = get_node("fade")
onready var endtimer  = get_node("EndTimer")
onready var camera    = player.get_node("camera")
onready var vorpal    = get_node("/root/soundtrack")

var time = 0
var beat
var fade_in_ended
var zone
var blackhole = Vector2(0,0)
var total_rescued = 0

signal ending

func _ready():
  Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
  randomize()
  bgimg.show()
  fade_in_ended = false
  beat = vorpal.event_instance("damnation")
  vorpal.push_command(beat, "noise-start")
  vorpal.push_command(beat, "beat-start")
  vorpal.push_command_1f(beat, "total-rescued", 0)
  vorpal.push_command(beat, "welcome-start")
  set_process(true)
  fade.set_color(Color(0,0,0,1))
  fade.fadein()
  zone = DARK_ZONE

func allocation_cycle():
  for t in range(100):
    yield(get_tree(), "fixed_frame")
  for i in range(OBSTACLE_NUM):
    for t in range(50):
      yield(get_tree(), "fixed_frame")
    var obstacle = Obstacle.instance()
    obstacle.add_to_group("obstacles")
    endtimer.connect("timeout", obstacle, "_blackhole_time")
    add_child(obstacle)
    obstacle.allocate(player.get_pos())
  while zone < FINAL_ZONE:
    for obstacle in get_tree().get_nodes_in_group("obstacles"):
      if zone >= FINAL_ZONE:
        return
      obstacle.allocate(player.get_pos())
      for t in range(100):
        yield(get_tree(), "fixed_frame")

func _fade_in_ended(_1, _2):
  fade_in_ended = true
  vorpal.push_command(beat, "beat-start")
  vorpal.push_command_1f(beat, "total-rescued", 0)
  vorpal.push_command(beat, "welcome-start")

func _blackhole_arrival():
  var angle = PI*2.0*randf()
  var dir = 50*Vector2(cos(angle), sin(angle))
  blackhole = player.get_pos() + dir;
  bgimg.blackhole_arrival(blackhole)
  zone = FINAL_ZONE
  tween.interpolate_method(self, "_grow_blackhole", 0.0, 1.0, 25.0, Tween.TRANS_QUART, Tween.EASE_OUT, 0.0)
  tween.start()
  vorpal.push_command(beat, "drum-end")
  vorpal.push_command(beat, "blackhole-start")

func _grow_blackhole(x):
  bgimg.grow_hole(x)
  var dir = blackhole - player.get_pos()
  var len = min(max(dir.length(), 0.01), 600)
  var force = 2000*x*dir.normalized()/len
  player.set_force(force)
  player.go_dark(x)

func _gameover():
  set_process(false)
  player.finish()
  vorpal = null
  get_tree().change_scene("res://scenes/gameover.tscn")

func _on_player_moving(dir):
  vorpal.push_command(beat, "beat-inc")

func _on_player_stopped():
  vorpal.push_command(beat, "beat-dec")

func _on_warden_rescued():
  total_rescued += 1
  vorpal.push_command_1f(beat, "total-rescued", total_rescued)
  if total_rescued >= 4:
    endtimer.start()
    vorpal.push_command(beat, "noise-stop")
    vorpal.push_command(beat, "beat-stop")
    emit_signal("ending")

func _on_transfer_zone():
  get_tree().change_scene("res://scenes/escape/main.tscn")

func _process(delta):
  bgimg.set_ref(camera.get_camera_pos(), 0, player.get_transform())
  if fade_in_ended:
    time += delta
    var osc = sin(2.0*PI*time*0.5)
    var scale = get_scale()
    camera.set_zoom(scale + 0.1*osc*Vector2(1,1))
  vorpal.tick(delta)
