
extends KinematicBody2D

const VAR = 1000

onready var vorpal       = get_node("/root/soundtrack")
onready var particles = get_node("particles")
onready var view      = get_node("view")
onready var player    = get_node("../player")

var sfx
var dir
var force = Vector2(0,0)

func _ready():
  var angle = randf()*2*PI
  dir = Vector2(cos(angle),sin(angle))
  set_pos(VAR*Vector2(rand_range(-1,1), rand_range(-1,1)))
  particles.set_param(Particles2D.PARAM_DIRECTION, rad2deg(-angle-PI/2.0))
  particles.show()
  show()
  sfx = vorpal.event_instance("satellite")
  get_parent().connect("start_beat", self, "_play")
  set_fixed_process(true)

func _exit_tree():
  vorpal.free_event(sfx)

func _play():
  vorpal.push_command(sfx, "start")

func set_force(f):
  force = f

func _fixed_process(delta):
  move(50*dir*delta + force)
  var pos = player.get_transform().basis_xform_inv(get_pos() - player.get_pos())/16
  vorpal.set_event_position(sfx, Vector3(pos.x, pos.y, 0))
