
extends Polygon2D

onready var tween    = get_node("tween")
onready var material = get_material()

var time = 0
var last_pos

func set_ref(pos,rot,xform):
  #pos /= 2*Vector2(1024,-600)
  #pos.y *= -1;
  last_pos = pos
  material.set_shader_param("pos", pos)
  material.set_shader_param("rot", -rot)
  material.set_shader_param("basex", xform.x)
  material.set_shader_param("basey", xform.y)

func _zone_transfer():
  for i in range(50):
    material.set_shader_param("zone", i/50.0)
    yield(get_tree(), "fixed_frame")

func blackhole_arrival(center):
  material.set_shader_param("hole", center)
  tween.interpolate_method(self, "grow_hole", 0.0, 1.0, 25.0, Tween.TRANS_QUAD, Tween.EASE_IN, 0.0)
  tween.start()

func grow_hole(x):
  material.set_shader_param("end", x)
