
extends Node2D

const Player = preload("res://scenes/damnation/player.gd")

const TELEPORT_DIST = 1600

const FREQ = (144.0/60.0/2.0)*2*PI

const HIDDEN = 0
const WAITING = 1
const RESCUED = 2

export(int, "Square", "Hexagon", "Pentagon", "Star") var type = 0
export(int, "Hidden", "Waiting") var state = HIDDEN

onready var shape = get_node("Shape")
onready var border = get_node("Border")
onready var tween = get_node("Tween")

signal rescued

var player_
var dist_
var angle_
var time_
var color_

func _ready():
  if type == 0:   # Square
    set_square()
  elif type == 1: # Hexagon
    set_hexagon()
  elif type == 2: # Pentagon
    set_pentagon()
  elif type == 3: # Star
    set_star()
  color_ = Color(1, 1, 1, 1)
  color_.s = 0.8
  color_.v = 0.5
  color_.h = randf()
  set_process(true)

func set_square():
  pass

func set_hexagon():
  var vtx = []
  vtx.resize(6)
  for i in range(6):
    var phase = i*PI/3.0
    vtx[i] = 16*Vector2(cos(phase), sin(phase))
  shape.set_polygon(Vector2Array(vtx))
  border.set_polygon(Vector2Array(vtx))

func set_pentagon():
  var vtx = []
  vtx.resize(5)
  for i in range(5):
    var phase = i*2.0*PI/5.0 + PI/2.0
    vtx[i] = 16*Vector2(cos(phase), sin(phase))
  shape.set_polygon(Vector2Array(vtx))
  border.set_polygon(Vector2Array(vtx))

func set_star():
  var vtx = []
  vtx.resize(10)
  for i in range(5):
    var phase = i*2.0*PI/5.0 - PI/2.0
    var mid = 2.0*PI/10.0
    vtx[2*i] = 16*Vector2(cos(phase), sin(phase))
    vtx[2*i + 1] = 8*Vector2(cos(phase + mid), sin(phase + mid))
  shape.set_polygon(Vector2Array(vtx))
  border.set_polygon(Vector2Array(vtx))

func teleport_to_player_path(player):
  var center = player.get_pos()
  var dir = -player.get_node("view").get_relative_transform_to_parent(player.get_parent()).y
  set_pos(center + TELEPORT_DIST*dir)
  printt("Repositioning Warden of type", type)

func _player_leave_area(player):
  if state == WAITING and player.get_script() == Player:
    teleport_to_player_path(player)

func set_player_distance(val):
  dist_ = val

func get_off():
  var off = Vector2(cos(angle_ + time_*FREQ), sin(angle_ + time_*FREQ))
  return dist_*off

func _on_grab(player):
  if state == WAITING and player.get_script() == Player:
    state = RESCUED
    player_ = player
    time_ = 0.0
    var diff = get_pos() - player.get_pos()
    dist_ = diff.length()
    angle_ = atan2(diff.y, diff.x)
    tween.interpolate_method(self, "set_player_distance", dist_, 80, 1.0, Tween.TRANS_CUBIC, Tween.EASE_IN)
    tween.start()
    emit_signal("rescued")
    set_fixed_process(true)

func _on_ending():
  tween.interpolate_method(self, "set_player_distance", 80, 0, 5.0, Tween.TRANS_CUBIC, Tween.EASE_IN, 15.0)
  tween.start()

func _fixed_process(delta):
  set_pos(player_.get_pos() + get_off())
  time_ += delta

func _process(delta):
  shape.set_color(color_)
  color_.h += delta
  color_.h = fmod(color_.h, 1)
