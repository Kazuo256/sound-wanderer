
extends Node2D

const ALLOCATION_RADIUS = 500

onready var vorpal     = get_node("/root/soundtrack")
onready var view    = get_node("view")
onready var body    = get_node("body")
onready var tween   = get_node("tween")
onready var player  = get_node("../player")
onready var speech  = vorpal.event_instance("obstacle")
onready var color   = view.get_color()

var highcolor

func _ready():
  highcolor = color
  highcolor.h += 0.2
  set_fixed_process(true)

func allocate(center):
  var n = 4 + 2*(randi()%3)
  assert(n%2==0)
  var vtx = Vector2Array()
  vtx.resize(n)
  for i in range(n):
    var sector = 2.0*PI/n
    var angle = sector*i + (1-2*randf())*sector/2
    var dist = 50 + (1-2*randf())*25
    var point = Vector2(dist*cos(angle), dist*sin(angle))
    vtx.set(i, point)
  var polyshape = ConcavePolygonShape2D.new()
  polyshape.set_segments(vtx)
  body.add_shape(polyshape)
  view.set_polygon(vtx)
  set_pos(center + ALLOCATION_RADIUS*Vector2(rand_range(-0.8,0.8), rand_range(-0.8,0.8)))
  tween.interpolate_method(view, "set_scale", Vector2(0.01, 0.01), Vector2(1,1), 0.5, Tween.TRANS_BACK, Tween.EASE_OUT)
  tween.start()
  vorpal.push_command(speech, "reset")

func _player_enter(body):
  if body == player:
    vorpal.push_command(speech, "contact")
    view.set_color(highcolor)

func _player_exit(body):
  if body == player:
    view.set_color(color)

func _blackhole_time():
  queue_free()

func _fixed_process(delta):
  var pos = player.get_transform().basis_xform_inv(get_pos() - player.get_pos())/16
  vorpal.push_command_1f(speech, "dist", 1.0-pos.length()/16)
  vorpal.set_event_position(speech, Vector3(pos.x, pos.y, 0))
