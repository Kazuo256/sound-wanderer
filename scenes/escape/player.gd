
extends KinematicBody2D

const SPEED = 4.0

onready var control = get_node("Control")
onready var view    = get_node("View")
onready var life    = 5
onready var dead    = false

var inclination = 0
var timer

signal death

func _ready():
  timer = Timer.new()
  timer.set_wait_time(2.0)
  timer.set_one_shot(true)
  timer.set_autostart(false)
  add_child(timer)
  set_fixed_process(true)

func take_damage():
  if life > 0:
    life -= 1
    view.remove_part()
  if life <= 0 and not dead:
    dead = true
    set_collision_mask(0)
    set_layer_mask(0)
    timer.start()
    yield(timer, 'timeout')
    emit_signal("death")

func _fixed_process(delta):
  var forward = Vector2(0, -1)
  var rightways = Vector2(1, 0)
  var impulse = control.get_impulse_2d(forward, rightways)
  inclination += (-impulse.x - inclination) * 0.1
  view.set_rot(inclination * PI/6.0)
  move(SPEED*impulse)
