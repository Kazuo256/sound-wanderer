
extends Node2D

const SPEED = 128

onready var vorpal  = get_node("/root/soundtrack")
onready var view    = get_node("view")
onready var body    = get_node("body")
onready var tween   = get_node("tween")
onready var timer   = get_node("DeathTimer")
onready var player  = get_node("../Player")
onready var color   = view.get_color()

var speed
var speech
var highcolor

func _ready():
  highcolor = color
  highcolor.h += 0.2
  speech = vorpal.event_instance("obstacle")
  speed = SPEED
  set_fixed_process(true)

func allocate(difficulty):
  speed = SPEED + 32*difficulty
  var n = 4 + 2*(randi()%3)
  assert(n%2==0)
  var vtx = Vector2Array()
  vtx.resize(n)
  for i in range(n):
    var sector = 2.0*PI/n
    var angle = sector*i + (1-2*randf())*sector/2
    var dist = 50 + (1-2*randf())*25
    var point = Vector2(dist*cos(angle), dist*sin(angle))
    vtx.set(i, point)
  var polyshape = ConcavePolygonShape2D.new()
  polyshape.set_segments(vtx)
  body.add_shape(polyshape)
  view.set_polygon(vtx)
  view.set_color(color)
  set_pos(Vector2(512 + 256*rand_range(-1,1), -32))
  tween.interpolate_method(view, "set_scale", Vector2(0.01, 0.01), Vector2(1,1), 0.5, Tween.TRANS_BACK, Tween.EASE_OUT)
  tween.start()
  vorpal.push_command(speech, "reset")
  vorpal.push_command(speech, "contact")
  set_fixed_process(true)

func _player_enter(body):
  if body == player:
    view.set_color(highcolor)
    body.take_damage()
    vorpal.push_command(speech, "die")
    tween.interpolate_method(view, "set_color", highcolor, Color(1,1,1,0), 0.1, Tween.TRANS_QUAD, Tween.EASE_IN)
    tween.start()
    timer.start()
    yield(timer, "timeout")
    set_fixed_process(false)
    set_pos(Vector2(512, -500))

func _player_exit(body):
  if body == player:
    view.set_color(color)

func _on_player_death():
  queue_free()

func _fixed_process(delta):
  var pos = (get_pos() - player.get_pos())/16
  vorpal.push_command_1f(speech, "dist", 1.0-pos.length()/128)
  vorpal.set_event_position(speech, Vector3(pos.x, 0, -pos.y))
  set_pos(get_pos() + delta*speed*Vector2(0, 1))
