extends Node2D

const OBSTACLE_NUM  = 4
const Obstacle      = preload("res://scenes/escape/obstacle.tscn")

onready var vorpal        = get_node("/root/soundtrack")
onready var player        = get_node("Player")
onready var timer         = get_node("ObstacleTimer")
onready var bg            = get_node("Background")
onready var difficulty    = 0
onready var next_obstacle = 0
onready var obstacles     = []

var bgm

func _ready():
  bgm = vorpal.event_instance("escape")
  vorpal.push_command(bgm, "start")
  for i in range(OBSTACLE_NUM):
    var obs = Obstacle.instance()
    obstacles.append(obs)
    add_child(obs)
    player.connect("death", obs, "_on_player_death")
    obs.set_pos(Vector2(0, -2000)) # out of screen
  set_process(true)

func _on_spawn_tick():
  var obs = obstacles[next_obstacle]
  obs.allocate(difficulty)
  next_obstacle = (next_obstacle + 1) % OBSTACLE_NUM
  timer.set_wait_time(max(0.2, 3*exp(-difficulty/10.0)))
  difficulty += 1
  bg.set_intensity(difficulty)

func _on_player_death():
  timer.disconnect("timeout", self, "_on_spawn_tick")
  set_process(false)
  yield(get_tree(), "fixed_frame")
  #vorpal.clear()
  yield(get_tree(), "fixed_frame")
  get_tree().change_scene("res://scenes/gameover.tscn")

func _process(delta):
  vorpal.tick(delta)
