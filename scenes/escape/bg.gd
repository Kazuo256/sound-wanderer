
extends Polygon2D

onready var material = get_material()

onready var time = 0
onready var intensity = 5
onready var intensity_target = 5

func _ready():
  set_fixed_process(true)

func set_intensity(value):
  intensity_target = 5 + value*2

func _fixed_process(delta):
  time += delta
  intensity += (intensity_target - intensity)*delta/100
  material.set_shader_param("time", time)
  material.set_shader_param("speed", intensity)
