
extends KinematicBody

const SPEED = 1.0/5.0
const GRAVITY = Vector3(0,-0.1,0)

onready var camera  = get_node("Camera")
onready var control = get_node("control")
onready var vorpal     = get_node("/root/soundtrack")
onready var camera_origin = camera.get_translation()

onready var height = get_translation().y
onready var rot = Vector2(0,0)
onready var step_time = 0
onready var alt_step = 1
onready var moving = false

var step_sfx

func _ready():
  get_parent().connect("game_ready", self, "_game_ready")

func _game_ready(_):
  set_fixed_process(true)
  step_sfx = vorpal.event_instance("step-sfx")
  assert(step_sfx >= 0)
  printt("step sfx created:", step_sfx)

func _fixed_process(delta):
  var rot = control.get_rot()
  if control.is_turning():
    vorpal.push_command(step_sfx, "turn")
  set_rotation(Vector3())
  rotate_y(rot)
  var forward = -get_transform().basis.z
  var rightways = get_transform().basis.x
  var impulse = control.get_impulse_3d(forward, rightways)
  if impulse.length_squared() > 0 and not moving:
    moving = true
    vorpal.push_command(step_sfx, "start")
  elif impulse.length_squared() == 0 and moving:
    moving = false
    vorpal.push_command(step_sfx, "stop")
  if moving:
    impulse.y = 0
    move(SPEED*impulse)
    step_oscillation(impulse.length(), delta)
  var floor_pos = get_translation()
  floor_pos.y = height
  move_to(floor_pos)

func step_oscillation(intensity, delta):
  step_time = fmod(step_time + intensity*delta, 2*PI)
  var step = Vector3(0,0,0)
  step.y = abs(0.4*cos(step_time*5))
  step.x = 0.4*sin(step_time*5)
  if step.y < 0.1and step_sfx != null:
    if step.x < 0 and alt_step == 0:
      vorpal.push_command(step_sfx, "mid")
      alt_step = 1 - alt_step
    elif step.x > 0 and alt_step == 1:
      vorpal.push_command(step_sfx, "mid")
      alt_step = 1 - alt_step
  camera.set_translation(camera_origin + step)

func teleport_to(pos):
  set_translation(pos)
  control.reset()
  vorpal.push_command(step_sfx, "teleport")


func _finish():
  set_fixed_process(false)
  vorpal.free_event(step_sfx)
