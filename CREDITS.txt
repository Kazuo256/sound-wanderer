
# Game Design

Wilson Kazuo Mizutani
Dino Vicente
Fabio Kon

# Programming

Wilson Kazuo Mizutani

# Pixel Art & 3D Models

Wilson Kazuo Mizutani

# Sound Design

Dino Vicente

# Voice Over

Yuri Koster

# Assets

Inconsolata Font from Google fonts under SIL Open Font License 1.1
(https://www.google.com/fonts/specimen/Inconsolata)

Model textures by Textures.com
(http://www.textures.com/)

PlayStation button icons by Daniele De Santis under CC-BY 4.0
(http://www.iconarchive.com/show/playstation-flat-icons-by-danieledesantis.html)

Sound samples by Dino Vicente
Except for the drum samples, which are licensed for use.
