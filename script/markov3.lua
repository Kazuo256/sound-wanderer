
local input_path, id, arity = ...

arity = tonumber(arity) or 1
local debug = true

local log
if debug then
  function log(...)
    print(string.format(...))
  end
else
  function log() end
end

local melody_track = {
  mario = {
    { ts = 0, track = 2 }
  },
  tetris = {
    { ts = 0, track = 2 }
  },
  undertale = {
    { ts = 0, track = 1 },
    { ts = 8*960, track = 2 },
    { ts = 16*960, track = 1 },
    { ts = 24*960, track = 2 },
    { ts = 32*960, track = 1 }
  }
}

local function isMelodyTrack (ts,track)
  --return track == 1
  local idx = #melody_track[id]
  for i,v in ipairs(melody_track[id]) do
    if v.ts > ts then
      idx = i-1
      break
    end
  end
  local expected_track = melody_track[id][idx].track
  return track == expected_track
end

-- Open input

local input_file = io.open(input_path, 'r')

assert(input_file, "Could not open input file")

-- Parse CSV into a timeline

local input = { {}, {} }
local timeline = {}
local ts_map = {}
local melody_part = 1

for line_str in input_file:lines() do
  local token_list = {}
  for token in line_str:gmatch('([^, "]+)') do
    table.insert(token_list, token)
  end
  local track = tonumber(token_list[1])
  local cmd = token_list[3]
  if track >= 1 and track <= 3 and cmd:match("^Note") then
    local timestamp = tonumber(token_list[2])
    local note_midi = tonumber(token_list[5])
    local speed = tonumber(token_list[6])
    local on = (cmd == "Note_on_c" and speed > 0)
    assert(note_midi and speed)
    track = isMelodyTrack(timestamp, track) and 1 or 2
    table.insert(input[track], {ts = timestamp, midi = note_midi, on = on})
  end
  table.sort(input[1], function (a,b) return a.ts < b.ts end)
  table.sort(input[2], function (a,b) return a.ts < b.ts end)
end

local function streamInput ()
  local i,j = 1,1
  while i <= #input[1] and j <= #input[2] do
    log("[%d,%d]", i, j)
    if input[1][i].ts <= input[2][j].ts then
      coroutine.yield(1, input[1][i])
      i = i + 1
    else
      coroutine.yield(2, input[2][j])
      j = j + 1
    end
  end
  while i <= #input[1] do
    coroutine.yield(1, input[1][i])
    i = i + 1
  end
  while j <= #input[2] do
    coroutine.yield(2, input[2][j])
    j = j + 1
  end
end

for track,event in coroutine.wrap(streamInput) do
  local note_midi = event.midi
  local timestamp = event.ts
  if event.on then
    log("[note on] %8d/%d: %2d", timestamp, track, note_midi)
    local entry
    if ts_map[timestamp] then
      -- Update timestamp entry
      entry = timeline[ts_map[timestamp]]
    else
      -- Insert new timestamp entry
      local idx = 1 -- last smaller than or equal to current ts
      while idx <= #timeline and timeline[idx].ts < timestamp do
        idx = idx + 1
      end
      log("[note on] unaligned %d/%d %4d", idx, #timeline, timestamp)
      if track == 2 then
        entry = timeline[math.max(1,idx-1)]
      else
        entry = { ts=timestamp, main={}, notes={} }
        table.insert(timeline, idx, entry)
        ts_map[timestamp] = #timeline
        for up=idx+1,#timeline do
          ts_map[timeline[up].ts] = up
        end
        log("new timeline entry %8d (%d)", timestamp, ts_map[timestamp])
      end
    end
    if track == 1 and not entry.main.midi then
      entry.main = { midi=note_midi, len=500 }
      log("[note on] on melody track")
    else
      if track == 1 and entry.main.midi < note_midi then
        entry.main.midi, note_midi = note_midi, entry.main.midi
      end
      table.insert(entry.notes, {
        midi = note_midi,
        len = 500,
        delay = timestamp - entry.ts
      })
      table.sort(entry.notes, function (a,b) return a.midi < b.midi end)
    end
  else
    log("[note off] %8d/%d: %2d", timestamp, track, note_midi)
    local entry_idx = #timeline
    while timeline[entry_idx].ts >= timestamp do
      entry_idx = entry_idx - 1
    end
    if track == 1 then
      log("[note off] on melody track")
      while entry_idx > 0 do
        if timeline[entry_idx].main.midi == note_midi then
          break
        end
        entry_idx = entry_idx - 1
      end
      if entry_idx > 0 then
        local entry = timeline[entry_idx]
        entry.main.len = timestamp - entry.ts
      end
    else
      local note_idx
      while entry_idx > 0 do
        local entry = timeline[entry_idx]
        for i,note in ipairs(entry.notes) do
          if note.midi == note_midi and timestamp > entry.ts+note.delay then
            note_idx = i
            goto found
          end
        end
        entry_idx = entry_idx - 1
      end
      ::found::
      if entry_idx > 0 then
        local entry = timeline[entry_idx]
        local note = entry.notes[note_idx]
        assert(entry.ts+note.delay < timestamp,
               "(" .. entry_idx .. ")" .. entry.ts .. " >= " .. timestamp)
        note.len = timestamp - (entry.ts+note.delay)
      end
    end
  end
  log("%d", #timeline)
  ::skip::
end

if debug then
  for idx,entry in ipairs(timeline) do
    assert(entry.main.midi, entry.ts)
    local str = string.format("%3d:%8d", idx, entry.ts)
    str = str .. string.format(" (%2d:%4d)", entry.main.midi,
                                             entry.main.len)
    for i,note in ipairs(entry.notes) do
      str = str .. string.format(" (%2d:%4d:%4d)", note.midi, note.len,
                                                   note.delay)
    end
    print(str)
  end
end

-- Collect states

local function hash_note(note)
  note = note or { midi = 0, len = 0, delay = 0 }
  assert(note.len >= 0, "negative length: " .. note.len)
  assert(note.delay >= 0, "negative delay: " .. note.delay)
  return string.pack("bIH", note.midi, note.len, note.delay)
end

local state_set = {}
local state_sequence = {}
local state_map = {}
local transition_set = {}
local transition_map = {}

for idx,entry in ipairs(timeline) do
  local state = { main=nil, notes={} }
  if idx > 1 then
    local last = timeline[idx-1]
    state.main = {
      midi   = entry.main.midi - last.main.midi,
      len    = entry.main.len,
      delay  = entry.ts - last.ts
    }
    for _,note in ipairs(entry.notes) do
      table.insert(state.notes, {
        midi  = note.midi - entry.main.midi,
        len   = note.len,
        delay = note.delay
      })
    end
  end
  local state_hash = hash_note(state.main)
  for _,note in ipairs(state.notes) do
    state_hash = state_hash .. hash_note(note)
  end
  table.insert(state_sequence, state_hash)
  if not state_set[state_hash] then
    table.insert(state_map, state_hash)
    state_set[state_hash] = {
      state = state, idx = #state_map
    }
  end
  -- Calculate transition
  local hash = ""
  local transition_seq = {}
  for i=1,arity do
    if i < #state_sequence then
      local last_hash = state_sequence[#state_sequence-i]
      hash = last_hash .. hash
      table.insert(transition_seq, state_set[last_hash].idx)
    else
      hash = "0" .. hash
      table.insert(transition_seq, 0)
    end
  end
  local transition = transition_set[hash]
  if not transition then
    table.insert(transition_map, hash)
    transition = {
      seq = transition_seq, idx = #transition_map, dist = {}, count = 0
    }
    transition_set[hash] = transition
  end
  transition.dist[state_hash] = (transition.dist[state_hash] or 0) + 1
  transition.count = transition.count + 1
end

local function transitionId (transition)
  local id = 0
  for i=#transition.seq,1,-1 do
    id = id * #state_map
    id = id + transition.seq[i]
  end
  return id
end

if debug then
  for idx,state_hash in ipairs(state_map) do
    local state = state_set[state_hash].state
    local str = string.format("%3d:", idx)
    if state.main then
      str = str .. string.format(" (%2d:%4d:%4d)", state.main.midi,
                                                   state.main.len,
                                                   state.main.delay)
      for i,note in ipairs(state.notes) do
        str = str .. string.format(" (%2d:%4d:%4d)", note.midi, note.len,
                                                     note.delay)
      end
    end
    print(str) -- .. "(" .. state_hash .. ")")
  end
  for idx,transition_hash in ipairs(transition_map) do
    local transition = transition_set[transition_hash]
    local str = string.format("%6d: ", transitionId(transition))
    for i=1,arity do
      str = str .. string.format(" (%3d)", transition.seq[i] or 0)
    end
    print(str) -- .. "(" .. state_hash .. ")")
  end
  print("Timeline length: "..#timeline) 
  print("Total states: "..#state_map) 
  print("Total transitions: "..#transition_map) 
end

do -- Generate state table

  local output = io.open(id.."-states.txt", 'w')
  output:write("markov-state-count-"..id.." "..#state_map..';\n')
  local fmt = "markov-state-%s-"..id.."-%d %d %d;\n"
  for idx,state_hash in ipairs(state_map) do
    local dist = state_set[state_hash]
    do
      local note = dist.state.main or {midi=0,len=0,delay=0}
      output:write(fmt:format('note', 1, note.midi or 0, dist.idx-1))
      output:write(fmt:format('len', 1, note.len or 0, dist.idx-1))
      output:write(fmt:format('delay', 1, note.delay or 0, dist.idx-1))
    end
    for i,note in ipairs(dist.state.notes) do 
      --if i <= 3 then -- FIXME
        output:write(fmt:format('note', (i+1), note.midi or 0, dist.idx-1))
        output:write(fmt:format('len', (i+1), note.len or 0, dist.idx-1))
        output:write(fmt:format('delay', (i+1), note.delay or 0, dist.idx-1))
      --end
    end
  end
  output:close()

end

do -- Generate transition table
  
  local output = io.open(id.."-transitions.txt", 'w')
  local fmt = "markov-"..id.."-%s %s;\n"
  output:write(fmt:format("arity", tostring(arity)))
  output:write(fmt:format("size", tostring(#state_map)))
  output:write(fmt:format("state", tostring(0)))
  fmt = "markov-"..id.."-set %d"
  for idx,transition_hash in ipairs(transition_map) do
    local transition = transition_set[transition_hash]
    output:write(fmt:format(transitionId(transition)))
    local transitions = " 0.0"
    local total = transition.count
    if total > 0 then
      for _,other in ipairs(state_map) do
        transitions = transitions .. " " .. (transition.dist[other] or 0)/total
      end
    else
      for _,other in ipairs(state_map) do
        transitions = transitions .. " " .. tostring(1/#state_map)
      end
    end
    output:write(transitions .. ';\n')
  end
  
end

