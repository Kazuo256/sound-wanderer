
local input_path, id, arity = ...

arity = arity or 1
id = id or 'tetris'
local debug = true

local log
if debug then
  function log(...)
    print(...)
  end
else
  function log() end
end

-- Open input

local input_file = io.open(input_path, 'r')

assert(input_file, "Could not open input file")

-- Parse CSV into a timeline

local timeline = {}
local ts_map = {}

for line_str in input_file:lines() do
  local token_list = {}
  for token in line_str:gmatch('([^, "]+)') do
    table.insert(token_list, token)
  end
  local note_midi = tonumber(token_list[5])
  local timestamp = tonumber(token_list[2])
  if token_list[3] == "Note_on_c" then
    log("[note on]", timestamp, note_midi)
    local entry
    if ts_map[timestamp] then
      -- Update timestamp entry
      entry = timeline[ts_map[timestamp]]
    else
      -- Insert new timestamp entry
      entry = { ts=timestamp, notes={} }
      local idx = 1
      while idx <= #timeline and timeline[idx].ts < timestamp do
        idx = idx + 1
      end
      table.insert(timeline, idx, entry)
      ts_map[timestamp] = #timeline
      for up=idx+1,#timeline do
        ts_map[timeline[up].ts] = up
      end
    end
    local unique = true
    for _,note in ipairs(entry.notes) do if note.midi == note_midi then
      unique = false
    end end
    if unique then
      table.insert(entry.notes, {midi=note_midi,len=50})
      table.sort(entry.notes, function (a,b) return a.midi < b.midi end)
    end
  elseif token_list[3] == "Note_off_c" then
    log("[note off]", timestamp, note_midi)
    local entry_idx = #timeline
    while timeline[entry_idx].ts >= timestamp do
      entry_idx = entry_idx - 1
    end
    local note_idx
    while entry_idx > 0 do
      for i,note in ipairs(timeline[entry_idx].notes) do
        if note.midi == note_midi then
          note_idx = i
          goto found
        else
          --log("[note off] mismatch",
          --    entry_idx, note_midi .. "~=" .. note.midi)
        end
      end
      entry_idx = entry_idx - 1
    end
    ::found::
    assert(entry_idx > 0)
    local entry = timeline[entry_idx]
    assert(entry.ts < timestamp,
           "(" .. entry_idx .. ")" .. entry.ts .. " >= " .. timestamp)
    entry.notes[note_idx].len = timestamp - entry.ts
  end
  log(#timeline)
  ::skip::
end

if debug then
  for idx,entry in ipairs(timeline) do
    local str = string.format("%3d: %8d", idx, entry.ts)
    for i,note in ipairs(entry.notes) do
      str = str .. string.format(" (%2d:%4d)", note.midi, note.len)
    end
    log(str)
  end
end

-- Collect states

local function hash_note(note)
  note = note or { midi = 0, len = 0 }
  assert(note.len >= 0, "negative length: " .. note.len)
  return string.pack("bH", note.midi, note.len)
end

local state_set = {}
local state_sequence = {}
local state_map = {}

for idx,entry in ipairs(timeline) do
  local state = { delay=0, notes={} }
  if idx > 1 then
    local last = timeline[idx-1]
    state.delay = entry.ts - last.ts
    local level = #entry.notes
    local last_level = #last.notes
    while not last.notes[last_level] do
      last_level = last_level - 1
    end
    state.notes[1] = {
      midi = entry.notes[level].midi - last.notes[last_level].midi,
      len = entry.notes[level].len
    }
    for idx=#entry.notes-1,1,-1 do
      table.insert(state.notes, {
        midi = entry.notes[idx].midi - entry.notes[level].midi,
        len = entry.notes[idx].len
      })
    end
  end
  local state_hash = string.pack("H", state.delay)
                     .. hash_note(state.notes[1])
                     .. hash_note(state.notes[2])
                     .. hash_note(state.notes[3])
  table.insert(state_sequence, state_hash)
  if not state_set[state_hash] then
    table.insert(state_map, state_hash)
    state_set[state_hash] = {
      state = state, idx = #state_map, dist = {}, count = 0
    }
  end
  if idx > 1 then
    local last = state_set[state_sequence[#state_sequence-1]]
    last.dist[state_hash] = (last.dist[state_hash] or 0) + 1
    last.count = last.count + 1
  end
end

if debug then
  for idx,state_hash in ipairs(state_map) do
    local state = state_set[state_hash].state
    local str = string.format("%3d: %4d", idx, state.delay)
    for i,note in ipairs(state.notes) do
      str = str .. string.format(" (%2d:%4d)", note.midi, note.len)
    end
    log(str) -- .. "(" .. state_hash .. ")")
  end
  log("Timeline length: "..#timeline) 
  log("Total states: "..#state_map) 
end

do -- Generate state table

  local output = io.open(id.."-states.txt", 'w')
  output:write("markov-state-"..id.."-count "..#state_map..';\n')
  local fmt = "markov-state-"..id.."-%s %d %d;\n"
  for idx,state_hash in ipairs(state_map) do
    local dist = state_set[state_hash]
    output:write(fmt:format('delay', dist.state.delay, dist.idx-1))
    for i,note in ipairs(dist.state.notes) do 
      if i <= 3 then -- FIXME
        output:write(fmt:format('note'..i, note.midi or 0, dist.idx-1))
        output:write(fmt:format('len'..i, note.len or 0, dist.idx-1))
      end
    end
  end
  output:close()

end

do -- Generate transition table
  
  local output = io.open(id.."-transitions.txt", 'w')
  output:write("markov-"..id.."-arity ".. arity ..";\n")
  output:write("markov-"..id.."-size "..#state_map..";\n")
  output:write("markov-"..id.."-state ".. 0 ..";\n")
  local fmt = "markov-"..id.."-set %d"
  for idx,state_hash in ipairs(state_map) do
    local state_entry = state_set[state_hash]
    output:write(fmt:format(state_entry.idx-1))
    local transitions = ""
    local total = state_entry.count
    if total > 0 then
      for _,other in ipairs(state_map) do
        transitions = transitions .. " " .. (state_entry.dist[other] or 0)/total
      end
    else
      for _,other in ipairs(state_map) do
        transitions = transitions .. " " .. tostring(1/#state_map)
      end
    end
    output:write(transitions .. ';\n')
  end
  
end

