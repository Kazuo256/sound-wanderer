
local input_path, id, arity = ...

id = id or 'mario'
arity = arity or 3

-- Open input

local input_file = io.open(input_path, 'r')

assert(input_file, "Could not open input file")

-- Parse CSV

local timeline = {}

table.insert(timeline, {note=0, harmony = {}, step = 0, delay = 0})

local last_timestamp = -1

for line_str in input_file:lines() do
  local token_list = {}
  for token in line_str:gmatch('([^, "]+)') do
    table.insert(token_list, token)
  end
  if token_list[1] == "2" and token_list[3] == "Note_on_c" then
    local last = timeline[#timeline]
    local note = tonumber(token_list[5])
    local timestamp = tonumber(token_list[2])
    local entry
    if timestamp > last_timestamp then
      entry = {
        note = note, harmony = {}, step = note - last.note,
        delay = timestamp - last_timestamp
      }
      table.insert(timeline, entry)
    else
      entry = last
      if note > entry.note then
        table.insert(entry.harmony, entry.note)
        table.sort(entry.harmony)
        entry.note = note
        entry.step = note - timeline[#timeline-1].note
      else
        table.insert(entry.harmony, note)
        table.sort(entry.harmony)
      end
    end
    last_timestamp = timestamp
  end
end

-- Collect states

local states_set = {}
local state_sequence = {}
local state_map = {}
for idx,entry in ipairs(timeline) do
  local state = string.pack("BBBbH", entry.note, entry.harmony[1] or 0,
                                     entry.harmony[2] or 0, entry.step,
                                     entry.delay)
  table.insert(state_sequence, state)
  if not states_set[state] then
    table.insert(state_map, state)
    states_set[state] = { entry = entry, idx = #state_map }
  end
  if idx > 1 then
    local last_state_dist = states_set[state_sequence[idx-1]]
    if not last_state_dist[state] then
      last_state_dist[state] = 0
    end
    last_state_dist[state] = last_state_dist[state] + 1
  end
end

--for idx,entry in ipairs(timeline) do
--  print(string.format("%3d: %2d %8d %2d", idx, entry.note, entry.delay,
--                                          entry.step),
--        table.unpack(entry.harmony))
--end

do -- Generate state table

  local states_output = io.open(id.."-states.txt", 'w')
  states_output:write("markov-state-count "..#state_map..';\n')
  local fmt = "markov-state-%s %d %d;\n"
  for state,dist in pairs(states_set) do
    states_output:write(fmt:format('note', dist.entry.note, dist.idx-1))
    states_output:write(fmt:format('delay', dist.entry.delay, dist.idx-1))
    states_output:write(fmt:format('harm1', dist.entry.harmony[1] or 0,
                                            dist.idx-1))
    states_output:write(fmt:format('harm2', dist.entry.harmony[2] or 0,
                                            dist.idx-1))
  end
  states_output:close()

end

do -- Generate transition table
  
  local transition_output = io.open(id.."-transitions.txt", 'w')
  transition_output:write("markov-arity ".. 1 ..";\n")
  transition_output:write("markov-size "..#state_map..";\n")
  transition_output:write("markov-state ".. 1 ..";\n")
  local fmt = "markov-set %d"
  for idx,state in ipairs(state_map) do
    local dist = states_set[state]
    local total = 0
    for other,count in pairs(dist) do
      if other ~= 'entry' and other ~= 'idx' then
        total = total + count
      end
    end
    assert(total > 0)
    transition_output:write(fmt:format(dist.idx-1))
    local transitions = ""
    for _,other in ipairs(state_map) do
      transitions = transitions .. " " .. (dist[other] or 0)/total
    end
    transition_output:write(transitions .. ';\n')
  end
  
end

